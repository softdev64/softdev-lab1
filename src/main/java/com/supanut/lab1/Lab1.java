package com.supanut.lab1;

import java.util.Scanner;

public class Lab1 {

    public static void main(String[] args) {
        char[][] Game = new char[3][3];
        for (int row = 0; row < Game.length; row++) {
            for (int col = 0; col < Game[row].length; col++) {
                Game[row][col] = '-';
            }
        }

        char player = 'X';
        int Draw = 0;
        boolean gameEnd = false;
        Scanner scanner = new Scanner(System.in);

        while (!gameEnd) {
            printGame(Game);
            System.out.print("Turn Player " + player + " Enter : ");
            int row = scanner.nextInt();
            int col = scanner.nextInt();
            System.out.println();

            if (Game[row][col] == '-') {
                Game[row][col] = player;
                gameEnd = Win(Game, player);
                if (gameEnd) {
                    System.out.println("Player " + player + " WIN ! ");
                } else {
                    Draw += 1;
                    player = (player == 'X') ? 'O' : 'X';
                    if (Draw == 9) {
                        System.out.println("! ! ! D R A W ! ! !");
                        printGame(Game);
                        System.exit(0);
                    }
                }
            } else {
                System.out.println("Incorrect number, Please enter again.");
            }
        }
        printGame(Game);
    }

    public static boolean Win(char[][] Game, char player) {
        for (int row = 0; row < Game.length; row++) {
            if (Game[row][0] == player && Game[row][1] == player && Game[row][2] == player) {
                return true;
            }
        }

        for (int col = 0; col < Game.length; col++) {
            if (Game[0][col] == player && Game[1][col] == player && Game[2][col] == player) {
                return true;
            }
        }

        if (Game[0][0] == player && Game[1][1] == player && Game[2][2] == player) {
            return true;
        }

        if (Game[0][2] == player && Game[1][1] == player && Game[2][0] == player) {
            return true;
        }
        return false;
    }

    public static void printGame(char[][] Game) {
        for (int row = 0; row < Game.length; row++) {
            for (int col = 0; col < Game[row].length; col++) {
                System.out.print(Game[row][col] + " ");
            }
            System.out.println();
        }
    }

}
